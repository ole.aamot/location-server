const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
const port = 3000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/location-server-time', async (req, res) => {
    const { timeOut, timeBegin, timeEnd } = req.query;
    try {
	const response = new Date().getTime();
        res.json(response);
    } catch (error) {
        res.status(500).json({ error: 'Error fetching time' });
    }
});

app.get('/location-server-data', async (req, res) => {
    const { service, glat, glon, radius } = req.query;
    try {
        const response = await axios.get(`https://api.piperpal.com/location/json.php?service=${service}&glat=${glat}&glon=${glon}&radius=${radius}`);
	res.json(response.data);
        const locations = response.data.locations;
    } catch (error) {
        res.status(500).json({ error: 'Error fetching data' });
    }
});

app.listen(port, () => {
    console.log(`Location Data Server listening on port ${port}`);
});
