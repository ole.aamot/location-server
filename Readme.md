### Location Data Server for NodeJS and Piperpal.com 10

In this project we will use the nodejs axios library to interact with the Piperpal.com apis

### Run the script

```bash
node location-server.cjs
```

### Endpoints

http://localhost:3000/location-server-time?timeOut=0&TimeBegin=2024-01-01&timeEnd=2024-12-31

http://localhost:3000/location-server-data?service=Clothes&location=http://www.stormberg.com/&glat=59.9292125&glon=10.7898633&radius=1000
